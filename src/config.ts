let global : any = typeof window !== 'undefined' ? window : self;
global.config = {
	mainnetExplorerUrl: "https://explorer.minesolo.com/",
	testnetExplorerUrl: "http://testnet.explorer.minesolo.com/",
	testnet: true,
	coinUnitPlaces: 9,
	txMinConfirms: 20,         // corresponds to CRYPTONOTE_DEFAULT_TX_SPENDABLE_AGE in Monero
	txCoinbaseMinConfirms: 1000, // corresponds to CRYPTONOTE_MINED_MONEY_UNLOCK_WINDOW in Monero
	addressPrefix: 13975,
	integratedAddressPrefix: 26009,
	addressPrefixTestnet: 13976,
	integratedAddressPrefixTestnet: 26112,
	subAddressPrefix: 23578,
	subAddressPrefixTestnet: 27947,
	defaultMixin: 13, // default value mixin

	coinSymbol: 'XSV',
	openAliasPrefix: "xsv",
	coinName: 'Solo',
	coinUriPrefix: 'solo:',
	avgBlockTime: 20,
	maxBlockNumber: 500000000,
};
