<?php

$daemonAddress = getenv('daemon_address') !== false ? getenv('daemon_address') : '127.0.0.1';
$rpcPort = getenv('daemon_rpc_port') !== false ? (int)getenv('daemon_rpc_port') : 23424;
$coinSymbol = 'xsv';
